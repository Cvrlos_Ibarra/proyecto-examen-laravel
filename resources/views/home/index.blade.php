@extends('home.template')
@section('content')
  
  @include('parcial.gestionRemota')


  <div class="row">
    <div class="col l2"></div>
    <div class="col l10 s12">
        <div class="col l6 s12">
            <div class="col l3 s12"></div>
            <div class="col l9 s12">
                <h5 class="green-text text-darken-2 alig">Funciones del navegador</h5>
                <p align="left"><font class="green-text
        text-darken-2">Kiosk Browser</font> tiene un amplio conjunto de funciones. Puede probar estas funciones
                    durante 5 días simplemente instalando, se requiere una licencia para uso personal y
                    comercial.</p>
                <div class="row">
                    <div class="col l5 s5">
                        <img class="responsive-img" src="img/google-play-badge.png">
                    </div>
                    <div class="col l7"></div>
                </div>
            </div>




        </div>
        <div class="col l6 s12">
            <img class="responsive-img left-align" src="img/candado.png">
        </div>


    </div>

    <div class="col l2"></div>


</div>
<br>
<div class="row">
    <div class="col l2"></div>
    <div class="col l10">
        <div class="col l5 s12">
            <img class="responsive-img right-align" src="img/candado2.png">
        </div>
        <div class="col l7 s12">
            <div class="col l9 s12">
                <h5 class="orange-text text-darken-4 alig">Funciones del lanzador</h5>
                <p align="left"><font class="orange-text
        text-darken-4">Kiosk Browser</font> tiene un amplio conjunto de funciones. Puede probar estas funciones
                    durante 5 días simplemente instalando, se requiere una licencia para uso personal y
                    comercial.</p>
                <input class="btn orange darken-3" type="button" value="Instalar">

            </div>
            <div class="col l3"></div>





        </div>



    </div>

    <div class="col l2"></div>


</div>



<br>
<div class="row">
    <div class="col l2"></div>
    <div class="col l8 s12">
        <h5 class="green-text darken-2">Opiniones de clientes</h5>
        <div class="col l6">
            <blockquote style="border-color: #388e3c">
                <p align="justify">Cuando comenzamos a trabajar con esta aplicación, tenía una curva de aprendizaje
                    bastante empinada. Estos chicos han estado trabajando diligentemente en la
                    construcción de su documentación, escuchando a sus clientes y agregando
                    correcciones / características adicionales; y ha valido la pena! Esta aplicación vale
                    cada centavo si está buscando usarla comercialmente como lo hemos hecho
                    nosotros</p>
                <p class="orange-text text-darken-4 alig">Gunther Vinson, TowMate LLC</p>
            </blockquote>

        </div>
        <div class="col l6">
            <blockquote style="border-color: #388e3c">
                <p align="justify">Gran producto, soporte increíble . Usamos este producto en aproximadamente 40
                    ubicaciones en nuestra empresa y funciona perfectamente todo el día, todos los días.</p>
                <p class="orange-text text-darken-4 alig">David Higginson</p>
            </blockquote>
            <blockquote style="border-color: #388e3c">
                <p align="justify">Perfecto , utilícelo para nuestro quiosco de reserva de canchas de tenis con una
                    aplicación web Google Apps Script. Funciona perfectamente.</p>
                <p class="orange-text text-darken-4 alig">Serge Gravelle</p>
            </blockquote>

        </div>
    </div>
    <div class="col l2"></div>


</div>




@endsection