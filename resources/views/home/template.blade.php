<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta
            name="viewport"
            content="width=device-width, initial-scale=1, maximum-scale=1.0"
    />
    <title>Plantilla inicial Materialize</title>

    <!-- CSS  -->
    <link
            href="https://fonts.googleapis.com/icon?family=Material+Icons"
            rel="stylesheet"
    />
    <link
            href="css/materialize.css"
            type="text/css"
            rel="stylesheet"
            media="screen,projection"
    />
    <link
            href="css/style.css"
            type="text/css"
            rel="stylesheet"
            media="screen,projection"
    />
</head>
<body>

<div class="navbar-fixed grey darken-3">
    <nav class="grey darken-3">
        <div class="container ">

            <a class="brand-logo hide-on-small-only green-text " href="index.html">App Bloqueo android</a>
            <a class="brand-logo show-on-small hide-on-large-only hide-on-med-only  " style="font-size: 5vw;"
               href="index.html">App Bloqueo android</a>

            <ul class="right hide-on-med-and-down">
                <li><a href="{{route('home')}}">Inicio</a></li>
                <li><a href="{{route('caracteristicas')}}">Características</a></li>
                <li><a href="{{'contacto'}}">Contacto</a></li>
            </ul>

            <ul id="nav-mobile" class="sidenav">


                <li><a href="{{route('home')}}">Inicio</a></li>
                <li>
                    <div class="divider"></div>
                </li>
                <li><a href="{{route('caracteristicas')}}">Características</a></li>
                <li><a href="{{'contacto'}}">Contacto</a></li>
            </ul>

            <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>

        </div>

    </nav>


</div>

@yield('content')



<footer class="page-footer grey darken-4">
    <div class="container ">
        <div class="row">
            <div class="col l6 s12">
                <h5>Contacto</h5>
                <p align="justify">Plaza IT oficina 3 sección diamante, 5 de mayo, Lagos de Moreno,Jalisco, México
                    soporte@android-app.com</p>
            </div>
            <div class="col l4 s12">
                <h5 align="center">Suscríbete</h5>
                <ul>
                    <li class="center-align"><i class="material-icons">facebook</i><i class="material-icons">rss_feed</i></li>

                </ul>

            </div>


        </div>


    </div>

    <div class="footer-copyright col l12 grey darken-3">
        <div class="center container">
            <i class="material-icons">copyright</i> Todos los derechos reservados 2020
        </div>
    </div>

</footer>

<!--  Scripts-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.slider');
        var instances = M.Slider.init(elems);
    });
</script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.collapsible');
        var instances = M.Collapsible.init(elems);
    });
</script>
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.materialboxed');
        var instances = M.Materialbox.init(elems, {
            inDuration: 2000,
            outDuration: 2000
        });
    });
</script>
</body>
</html>