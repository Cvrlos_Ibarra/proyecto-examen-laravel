<div class="row grey darken-3">
    <div class="col l2"></div>
    <div class="col l8">
        <div class="col l6">
            <br>
            <h5 class="white-text">Gestión Remota de bloqueo</h5>

            <p class="white-text">Administre todas sus instalaciones de <font class="green-text
        text-darken-2">Kiosk Browser</font> / <font class="orange-text text-darken-4">Kiosk
                Launcher</font> con nuestra consola
                de administración central.</p>
            <input class="btn green darken-2" type="button" value="Iniciar Sesión">
            <br>
            <br>
            <input class="btn orange darken-3" type="button" value="Registrarse">
        </div>
        <div class="col l6">
            <img class="responsive-img" src="img/escritorio.png">
            <br>
        </div>
        <br>

    </div>
    <div class="l2"></div>

</div>



