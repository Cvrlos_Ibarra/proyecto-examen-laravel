@extends('home.template')

@section('content')

@include('parcial.gestionRemota')

<div class="row">
    <div class="col l2"></div>
    <div class="col l8 s12">
    	<br>
        <h5>Características</h5>
        <div class="col l6 s12">
                <p align="justify">La administración remota está repleta de excelentes funciones para ayudarlo a
                    administrar y mantener sus dispositivos.</p>
            <h6>Perfiles de configuración <font class="white-text green
        darken-2">Navegador </font><font class="white-text orange
        darken-4">Lanzador</font></h6>
            <p>Envíe la misma configuración a grupos de dispositivos</p>


            <h6>Acciones de dispositivo push <font class="white-text green
        darken-2">Navegador </font><font class="white-text orange
        darken-4">Lanzador</font></h6>
            <p>Ejecute acciones inmediatas contra cada dispositivo, como reiniciar la aplicación,
                reiniciar *, etc.</p>

            <h6>Actualizaciones <font class="white-text green
        darken-2"> Navegador </font><font class="white-text orange
        darken-4"> Lanzador</font></h6>
            <p>Instale las actualizaciones del Kiosk Browser / Launcher de forma remota sin visitar
                sus dispositivos *</p>



        </div>
        <div class="col l6 s12">
            <img class="responsive-img" src="img/caracteristicas2.png">

        </div>
    </div>
    <div class="col l2"></div>


</div>



@endsection